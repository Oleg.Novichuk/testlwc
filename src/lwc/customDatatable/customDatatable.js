import LightningDatatable from 'lightning/datatable';
import customMenuTemplate from './customMenuTemplate.html';

export default class CustomDatatable extends LightningDatatable {
  static customTypes = {
    customMenu: {
      template: customMenuTemplate,
      standardCellLayout: true,
      typeAttributes: ['rowId'],
    }
  };
}