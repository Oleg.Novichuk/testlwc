import { LightningElement, wire, track } from 'lwc';
import getContactList from '@salesforce/apex/ContactListCtrl.getContactList';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

const columns = [{ label: 'First Name', fieldName: 'FirstName' },
                 { label: 'Last Name', fieldName: 'LastName' },
                 { label: 'Email', fieldName: 'Email', type: 'email' },
                 { label: '', fieldName: 'menu', type: 'customMenu',
                    typeAttributes: {
                      rowId: { fieldName: 'Id' },
                    }
                 }];

export default class ContactList extends LightningElement {
  columns = columns;

  @wire(getContactList)
  contact;

  handleShowDetail(event) {
    console.log(JSON.stringify(event.detail));
    const { rowId } = event.detail;
    this.showRowDetails(rowId);
  }

  @wire(CurrentPageReference) pageRef;
  showRowDetails(recordId) {
    fireEvent(this.pageRef, 'openDetail', recordId);
  }

}